use cc::Build;
use fs_extra as fse;
use std::collections::HashMap;
use std::fmt::Display;
use std::path::{Path, PathBuf};
use std::{env, fmt};
use walkdir::WalkDir;

//
pub const ENV_KEY_IDF_PATH: &str = "IDF_PATH";
/// The FREERTOS_SRC env variable must point to the FreeRTOS kernel code.
/// The Kernel can be found at Github: https://github.com/FreeRTOS/FreeRTOS-Kernel
///
/// When not set, you can use the Builder to specify the path. Must be an absolute path to the idf freertos component directory, and will be preferred over IDF_PATH
pub const ENV_KEY_FREERTOS_SRC: &str = "FREERTOS_SRC";

/// When not set, you can use the Builder to specify the path. The default is "device".
pub const ENV_KEY_FREERTOS_INCLUDE_DIR: &str = "FREERTOS_INCLUDE_DIR";

/// The FREERTOS_CONFIG variable must point to the directory
/// where the FreeRTOSConfig.h file is located for the current project.
///
/// When not set, you can use the Builder to specify the path relative to your crates src folder.
pub const ENV_KEY_FREERTOS_CONFIG: &str = "DEP_FREERTOS_CONFIG";

/// FreeRTOS shim.c file to enable usage of FreeRTOS with freertos-rust crate
/// This variable is set by esp32-freertos-rs build.rs
pub const ENV_KEY_FREERTOS_SHIM: &str = "DEP_FREERTOS_SHIM";

#[derive(Clone, Debug)]
pub struct Builder {
    freertos_dir: PathBuf,
    freertos_config_dir: PathBuf,
    freertos_include_dir: PathBuf,
    system_include_dirs: Vec<PathBuf>,
    freertos_shim: PathBuf,
    freertos_port: Option<String>,
    freertos_exclude: Vec<String>,
    defines: HashMap<String, String>,
    // name of the heap_?.c file
    heap_c: Option<String>,
    cc: Build,
}

pub struct Error {
    /// More explanation of error that occurred.
    message: String,
}

impl Error {
    fn new(message: &str) -> Error {
        Error {
            message: message.to_owned(),
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl Builder {
    /// Construct a new instance of a blank set of configuration.
    ///
    /// This builder is finished with the [`compile`] function.
    ///
    /// [`compile`]: struct.Build.html#method.compile
    pub fn new() -> Builder {
        let idf_path = env::var(ENV_KEY_IDF_PATH).unwrap_or_default();
        let freertos_path = env::var(ENV_KEY_FREERTOS_SRC);
        let freertos_include_dir =
            env::var(ENV_KEY_FREERTOS_INCLUDE_DIR).unwrap_or(String::from("include/freertos"));
        let freertos_config_path = env::var(ENV_KEY_FREERTOS_CONFIG).unwrap_or_default();
        let freertos_shim = env::var(ENV_KEY_FREERTOS_SHIM).unwrap_or_default();

        let freertos_path = freertos_path.unwrap_or(String::from(
            Path::new(idf_path.as_str())
                .join("components")
                .join("freertos")
                .to_str()
                .unwrap(),
        ));

        let b = Builder {
            freertos_dir: PathBuf::from(freertos_path),
            freertos_include_dir: PathBuf::from(freertos_include_dir),
            system_include_dirs: Vec::new(),
            freertos_config_dir: PathBuf::from(freertos_config_path),
            freertos_shim: PathBuf::from(freertos_shim),
            freertos_exclude: vec!["FreeRTOSConfig.h".into()],
            freertos_port: None,
            defines: HashMap::new(),
            cc: cc::Build::new(),
            heap_c: None,
        };
        return b;
    }

    /// Set the path to freeRTOS source
    /// Default is loaded from ENV variable "FREERTOS_SRC"
    pub fn freertos<P: AsRef<Path>>(&mut self, path: P) -> &mut Self {
        self.freertos_dir = path.as_ref().to_path_buf();
        self
    }
    /// Set the path to freeRTOSConfig.h
    /// Default is loaded from ENV variable, see: ENV_KEY_FREERTOS_CONFIG
    pub fn freertos_config<P: AsRef<Path>>(&mut self, path: P) -> &mut Self {
        self.freertos_config_dir = path.as_ref().to_path_buf();
        self
    }

    /// Set the path to shim.c (required for freertos-rust)
    /// Default is loaded from ENV variable, see: ENV_KEY_FREERTOS_SHIM
    pub fn freertos_shim<P: AsRef<Path>>(&mut self, path: P) -> &mut Self {
        self.freertos_shim = path.as_ref().to_path_buf();
        self
    }

    pub fn freertos_include_dir<P: AsRef<Path>>(&mut self, path: P) -> &mut Self {
        self.freertos_include_dir = path.as_ref().to_path_buf();
        self
    }

    pub fn system_include_dir<P: AsRef<Path>>(&mut self, path: P) -> &mut Self {
        self.system_include_dirs.push(path.as_ref().to_path_buf());
        self
    }

    pub fn define(&mut self, key: String, val: String) -> &mut Self {
        self.defines.insert(key, val);
        self
    }

    pub fn merge_include_dirs<P: AsRef<Path>>(&mut self, from: P, to: P, out: P) -> &mut Self {
        if let Err(err) = fse::dir::create_all(out.as_ref().to_path_buf(), true) {
            println!("cargo:warning=could not merge files: {}", err);
            return self;
        }
        let opts = fse::dir::CopyOptions {
            content_only: true,
            ..fse::dir::CopyOptions::default()
        };

        if let Err(err) = fse::dir::copy(
            from.as_ref().to_path_buf(),
            out.as_ref().to_path_buf(),
            &opts,
        ) {
            println!("cargo:warning=could not merge files: {}", err);
            return self;
        }

        if let Err(err) =
            fse::dir::copy(to.as_ref().to_path_buf(), out.as_ref().to_path_buf(), &opts)
        {
            println!("cargo:warning=could not merge files: {}", err);
            return self;
        }

        self.system_include_dir(out)
    }

    fn pwd() -> String {
        let dir = env::current_dir().unwrap();
        dir.to_str().unwrap().to_string()
    }

    /// Returns a list of all files in the shim folder
    fn freertos_shim_files(&self) -> Vec<String> {
        let files: Vec<_> = WalkDir::new(self.freertos_shim.as_path())
            .follow_links(false)
            .max_depth(1)
            .into_iter()
            .filter_map(|e| e.ok())
            .filter_map(|entry| {
                let f_name = entry.file_name().to_str().unwrap();

                if !f_name.starts_with("test_") && f_name.ends_with(".c") {
                    return Some(String::from(entry.path().to_str().unwrap()));
                }
                return None;
            })
            .collect();
        files
    }

    /// Returns a list of all FreeRTOS source files
    fn freertos_files(&self) -> Vec<String> {
        let files: Vec<_> = WalkDir::new(self.freertos_dir.as_path())
            .follow_links(false)
            .max_depth(1)
            .into_iter()
            .filter_map(|e| e.ok())
            .filter(|f| {
                !self
                    .freertos_exclude
                    .contains(&String::from(f.file_name().to_str().unwrap()))
            })
            .filter_map(|entry| {
                let f_name = entry.file_name().to_str().unwrap();

                if !f_name.starts_with("test_") && f_name.ends_with(".c") {
                    return Some(String::from(entry.path().to_str().unwrap()));
                }
                return None;
            })
            .collect();
        files
    }
    fn freertos_port_files(&self) -> Vec<String> {
        let files: Vec<_> = WalkDir::new(self.get_freertos_port_dir())
            .follow_links(false)
            .into_iter()
            .filter_map(|e| e.ok())
            .filter_map(|entry| {
                let f_name = entry.file_name().to_str().unwrap();

                if !f_name.starts_with("test_") && f_name.ends_with(".c") {
                    return Some(String::from(entry.path().to_str().unwrap()));
                }
                return None;
            })
            .collect();
        files
    }

    fn get_freertos_include_dir(&self) -> PathBuf {
        self.freertos_dir.join(self.freertos_include_dir.as_path())
    }

    fn get_system_include_dirs(&self) -> &Vec<PathBuf> {
        &self.system_include_dirs
    }

    fn get_freertos_config_dir(&self) -> PathBuf {
        Path::new(Self::pwd().as_str()).join(self.freertos_config_dir.as_path())
    }

    /// Set the heap_?.c file to use from the "/portable/MemMang/" folder.
    /// heap_1.c ... heap_5.c (Default: heap_4.c)
    /// see also: https://www.freertos.org/a00111.html
    pub fn heap<P: AsRef<Path>>(&mut self, file_name: String) -> &mut Self {
        self.heap_c = Some(file_name);
        self
    }

    /// Access to the underlining cc::Build instance to further customize the build.
    pub fn get_cc(&mut self) -> &mut Build {
        &mut self.cc
    }

    /// set the freertos port dir relativ to the esp-idf FreeRTOS directory
    /// e.g. "xtensa"
    ///
    /// If not set it will be the same as the esp-idf FreeRTOS.
    pub fn freertos_port(&mut self, port_dir: String) {
        self.freertos_port = Some(port_dir);
    }

    fn get_freertos_port_dir(&self) -> PathBuf {
        let base = self.freertos_dir.clone();
        if self.freertos_port.is_some() {
            return base.join(self.freertos_port.as_ref().unwrap());
        }
        base
    }

    fn heap_c_file(&self) -> Option<PathBuf> {
        match &self.heap_c {
            Some(heap_file) => Some(self.freertos_dir.join(heap_file.as_str())),
            None => None,
        }
    }
    fn shim_c_file(&self) -> PathBuf {
        self.freertos_shim.join("shim.c")
    }

    /// Check that all required files and paths exist
    fn verify_paths(&self) -> Result<(), Error> {
        if !self.freertos_dir.clone().exists() {
            return Err(Error::new(&format!(
                "Directory freertos_dir does not exist: {}",
                self.freertos_dir.to_str().unwrap()
            )));
        }
        let port_dir = self.get_freertos_port_dir();
        if !port_dir.clone().exists() {
            return Err(Error::new(&format!(
                "Directory freertos_port_dir does not exist: {}",
                port_dir.to_str().unwrap()
            )));
        }

        let include_dir = self.get_freertos_include_dir();
        if !include_dir.clone().exists() {
            return Err(Error::new(&format!(
                "Directory freertos_include_dir does not exist: {}",
                include_dir.to_str().unwrap()
            )));
        }

        let sys_include_dirs = self.get_system_include_dirs();
        for sysinc in sys_include_dirs.iter() {
            if !sysinc.clone().exists() {
                return Err(Error::new(&format!(
                    "Directory system_include_dir does not exist: {}",
                    sysinc.to_str().unwrap()
                )));
            }
        }

        // The heap implementation
        let heap_c = self.heap_c_file();
        match heap_c {
            Some(heap_path) => {
                if !heap_path.clone().exists() || !heap_path.clone().is_file() {
                    return Err(Error::new(&format!(
                        "File heap_?.c does not exist: {}",
                        heap_path.to_str().unwrap()
                    )));
                }
            }
            None => {
                println!("cargo:warning=No heap allocator has been defined");
            }
        }

        // Allows to find the FreeRTOSConfig.h
        if !self.get_freertos_config_dir().clone().exists() {
            return Err(Error::new(&format!(
                "Directory freertos_config_dir does not exist: {}",
                self.freertos_config_dir.to_str().unwrap()
            )));
        }

        // Add the freertos shim.c to support freertos-rust
        let shim_c = self.shim_c_file();
        if !shim_c.clone().exists() || !shim_c.clone().is_file() {
            return Err(Error::new(&format!(
                "File freertos_shim '{}' does not exist, missing esp32-freertos-rs dependency?",
                shim_c.clone().to_str().unwrap()
            )));
        }

        Ok(())
    }

    pub fn compile(&mut self) -> Result<(), Error> {
        let mut b = self.cc.clone();

        let path_error = self.verify_paths();
        if path_error.is_err() {
            return path_error;
        }

        // config dir, highes priority
        b.include(self.freertos_config_dir.clone());
        // System include dirs
        b.includes(self.get_system_include_dirs());
        // FreeRTOS header files
        b.include(self.get_freertos_include_dir());
        // FreeRTOS port header files (e.g. portmacro.h)
        b.include(self.get_freertos_port_dir());
        if let Some(heap_c) = self.heap_c_file() {
            b.file(heap_c);
        }
        self.freertos_files().iter().for_each::<_>(|f| {
            b.file(f);
        });
        self.freertos_port_files().iter().for_each::<_>(|f| {
            b.file(f);
        });
        self.freertos_shim_files().iter().for_each::<_>(|f| {
            b.file(f);
        });

        let def = &self.defines;

        def.into_iter().for_each(|(var, val)| {
            b.define(var.as_str(), val.as_str());
        });

        let res = b.try_compile("freertos");
        if res.is_err() {
            return Err(Error::new(&format!("{}", res.unwrap_err())));
        }

        Ok(())
    }
}

#[test]
fn test_with_idf_path_set_otherwise_defaults() {
    env::set_var(ENV_KEY_IDF_PATH, "/esp-idf");
    env::set_var(ENV_KEY_FREERTOS_CONFIG, "include/freertos");
    env::set_var("TARGET", "xtensa-esp32-none-elf");
    let b = Builder::new();
    assert_eq!(
        b.freertos_dir.to_str().unwrap(),
        "/esp-idf/components/freertos"
    );
    assert_eq!(
        b.get_freertos_include_dir().to_str().unwrap(),
        "/esp-idf/components/freertos/include/freertos"
    );
    assert_eq!(
        b.get_freertos_port_dir().to_str().unwrap(),
        "/esp-idf/components/freertos"
    );
    assert_eq!(
        b.get_freertos_config_dir().to_str().unwrap(),
        "/esp-idf/components/freertos/include/freertos"
    );
}
